import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
import numpy as np
import argparse

CLASS_NAMES = ["pink primrose", "hard-leaved pocket orchid", "canterbury bells",
    "sweet pea", "english marigold", "tiger lily", "moon orchid",
    "bird of paradise", "monkshood", "globe thistle", "snapdragon",
    "colt's foot", "king protea", "spear thistle", "yellow iris",
    "globe-flower", "purple coneflower", "peruvian lily", "balloon flower",
    "giant white arum lily", "fire lily", "pincushion flower", "fritillary",
    "red ginger", "grape hyacinth", "corn poppy", "prince of wales feathers",
    "stemless gentian", "artichoke", "sweet william", "carnation",
    "garden phlox", "love in the mist", "mexican aster", "alpine sea holly",""
    "ruby-lipped cattleya", "cape flower", "great masterwort", "siam tulip",
    "lenten rose", "barbeton daisy", "daffodil", "sword lily", "poinsettia",
    "bolero deep blue", "wallflower", "marigold", "buttercup", "oxeye daisy",
    "common dandelion", "petunia", "wild pansy", "primula", "sunflower",
    "pelargonium", "bishop of llandaff", "gaura", "geranium", "orange dahlia",
    "pink-yellow dahlia?", "cautleya spicata", "japanese anemone",
    "black-eyed susan", "silverbush", "californian poppy", "osteospermum",
    "spring crocus", "bearded iris", "windflower", "tree poppy", "gazania",
    "azalea", "water lily", "rose", "thorn apple", "morning glory",
    "passion flower", "lotus", "toad lily", "anthurium", "frangipani",
    "clematis", "hibiscus", "columbine", "desert-rose", "tree mallow",
    "magnolia", "cyclamen", "watercress", "canna lily", "hippeastrum",
    "bee balm", "ball moss", "foxglove", "bougainvillea", "camellia", "mallow",
    "mexican petunia", "bromelia", "blanket flower", "trumpet creeper",
    "blackberry lily"]
CLASS_NAMES_CHINESE = ["粉红报春花","硬叶袋装兰花","坎特伯雷铃铛",
     "香豌豆","英国万寿菊","虎百合","月亮兰",
     "极乐鸟","附子","地球蓟","金鱼草",
     "马驹的脚","国王普罗蒂亚","矛蓟","黄鸢尾",
     "金球花","紫色金光菊","秘鲁百合","气球花",
     "巨型白魔芋百合","火百合","枕形花","贝母",
     "红姜","葡萄风信子","罂粟花","威尔士亲王羽毛",
     "无茎龙胆","朝鲜蓟","甜威廉","康乃馨",
     "花园福禄考","雾中之恋","墨西哥紫苑","高山海冬青",
     "红唇卡特兰","海角花","大麦草","暹罗郁金香",
     "四旬斋玫瑰","巴比顿雏菊","水仙花","剑百合","一品红",
     "bolero deep blue","wallflower","万寿菊","毛茛","牛眼菊",
     "普通蒲公英","牵牛花","野生三色堇","报春花","向日葵",
     "天竺葵","兰达夫主教","gaura","天竺葵","橙色大丽花",
     "粉黄色的大丽花？","cautleya spicata","日本海葵",
     "黑眼苏珊","银灌木","加州罂粟","osteospermum",
     "春番红花","胡须鸢尾","白头翁","罂粟","杂色菊",
     "杜鹃花","睡莲","玫瑰","刺苹果","牵牛花",
     "西番莲","荷花","蟾蜍百合","红掌","占芭花",
     "铁线莲","木槿","耧斗菜","沙漠玫瑰","锦葵",
     "玉兰","仙客来","西洋菜","美人蕉百合","朱红",
     "蜜蜂花","球苔","毛地黄","九重葛","山茶花","锦葵",
     "墨西哥矮牵牛","凤梨花","毯子花","喇叭花",
     "黑莓百合"]
IMAGE_SIZE=[300,300]

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--model_path",
        "-m",
        type=str,
        required = True,
        help="载入预训练模型的文件夹路径",
    )
    parser.add_argument(
        "--image_path",
        "-i",
        type=str,
        required = True,
        help="识别目标图像文件路径",
    )
    args = parser.parse_args()
    return args


def predict(model, image, image_size, confidence_threshold):
    image = tf.cast(image, tf.float32)
    image = tf.image.resize_with_pad(image, image_size[1], image_size[0])
    image = tf.expand_dims(image, axis=0)
    image = tf.keras.applications.efficientnet.preprocess_input(image)
    logits = model(image)
    print(logits)
    probs = np.exp(logits.numpy()) / np.sum(np.exp(logits.numpy()), axis=-1, keepdims=True)
    print(probs)
    pred_id = np.argmax(probs, axis=-1)
    pred_id = pred_id.item()
    pred_name = CLASS_NAMES[pred_id]
    return pred_id, pred_name

if __name__ == '__main__':
    args = get_args()
    model = tf.saved_model.load(args.model_path)
    image = tf.keras.utils.load_img(args.image_path)
    confidence_threshold = 0.6
    flower_id, flower_name = predict(model, image, IMAGE_SIZE, confidence_threshold)
    # 输出结果
    print(flower_id)
    '''
    if flower_id==80:
        print(flower_name)
    else:
        print(flower_name)
    '''