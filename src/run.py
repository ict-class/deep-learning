import os
import tkinter as tk
from tkinter import filedialog
from PIL import ImageTk, Image
from predict import predict, CLASS_NAMES_CHINESE
import tensorflow as tf

IMAGE_SIZE=[300,300]

resources = [{'title':"Dok Champa Flower Recognition",
              'label_top': "Please select a photo and then click the \"Recognize\" button",
              'image_frame': "Flower Photo",
              'image_label': "No photo",
              'select_button': "Browse",
              'recognize_button': "Recognize",
              'output_frame': "Result",
              'filename1': "Select a photo",
              'filename2': "JPG Files",
              'Exception1': "Please select a photo first",
              'result1': "It is the Dok Champa Flower",
              'result2': "Not Dok Champa Flower.\n\rInstead, it is ",
              'Exception2': "Runtime error",

              },
             {'title':"ດອກຈຳປາຈຳປາn",
              'label_top': "ກະລຸນາເລືອກຮູບແລ້ວຄລິກທີ່ປຸ່ມ \"ຮັບຮູ້\"",
              'image_frame':  "ຮູບ​ພາບ​ຂອງ​ດອກ​ໄມ້",
              'image_label': "ບໍ່ມີຮູບ",
              'select_button': "ຄົ້ນຫາ",
              'recognize_button': "Recognize",
              'output_frame': "ຜົນໄດ້ຮັບ",
              'filename1': "ເລືອກຮູບ",
              'filename2': "ໄຟລ໌ JPG",
              'Exception1': "ກະລຸນາເລືອກຮູບກ່ອນ",
              'result1': "ດອກຈຳປາ!",
              'result2': "",
              'Exception2': "ເວລາແລ່ນຜິດພາດ",
              }]

class App:
    def __init__(self, master, model_path:str, lao:bool):
        # 设置语言
        self.language = 1 if lao==True else 0

        # 设置主窗口
        self.master = master
        self.master.title(resources[self.language]['title'])
        self.master.geometry("450x700")
        
        # 设置预训练模型的文件夹路径
        self.model_path = model_path
        
        # 设置顶部标签
        self.label_top = tk.Label(self.master, text=resources[self.language]['label_top'])
        self.label_top.pack(pady=20)

        # 设置图片显示框
        self.image_frame = tk.LabelFrame(self.master, text=resources[self.language]['image_frame'], width=320, height=320 )
        self.image_frame.pack(pady=20)
        self.image_frame.pack_propagate(False)
        self.image_label = tk.Label(self.image_frame, text=resources[self.language]['image_label'], width=300, height=300)
        self.image_label.pack(pady=20)

        # 设置“选择照片”按钮
        self.select_button = tk.Button(self.master, text=resources[self.language]['select_button'], command=self.select_image)
        self.select_button.pack(pady=10)

        # 设置“识别”按钮
        self.recognize_button = tk.Button(self.master, text=resources[self.language]['recognize_button'], command=self.recognize_image)
        self.recognize_button.pack(pady=10)

        # 设置输出框
        self.output_frame = tk.LabelFrame(self.master, text=resources[self.language]['output_frame'], width=320, height=100)
        self.output_frame.pack(pady=20)
        self.output_frame.pack_propagate(False)
        self.output_label = tk.Label(self.output_frame, text="", width=300, height=80)
        self.output_label.pack(pady=10)

    def select_image(self):
        # 弹出文件选择窗口
        self.filename = filedialog.askopenfilename(initialdir=".", title=resources[self.language]['filename1'], filetypes=((resources[self.language]['filename2'], "*.jpg"),))

        if self.filename:
            # 显示选择的图片
            self.image_label.config(text=os.path.basename(self.filename))
            self.image = Image.open(self.filename)
            self.image = self.image.copy().resize((self.image_label.winfo_width(), self.image_label.winfo_height()))
            self.image = ImageTk.PhotoImage(self.image)
            self.image_label.config(image=self.image)

            # 清空输出框
            self.output_label.config(text="")
        else:
            # 如果没有选择文件，清空图片显示框和输出框
            self.image_label.config(text=resources[self.language]['image_label'])
            self.image_label.config(image="")
            self.output_label.config(text="")

    def recognize_image(self):
        try:
            # 检查是否选择了图片
            if not self.filename:
                raise Exception(resources[self.language]['Exception1'])

            # 调用predict.py对图片进行识别
            model = tf.saved_model.load(os.path.abspath(self.model_path))
            image = tf.keras.utils.load_img(os.path.abspath(self.filename))
            flower_id, flower_name = predict(model, image, IMAGE_SIZE)
            # 输出结果
            if flower_id==80:
                result = resources[self.language]['result1']
            else:
                result = "{}{}".format(resources[self.language]['result2'], flower_name)
            # 显示识别结果
            self.output_label.config(text=result)
        except Exception as e:
            # 显示错误信息
            self.output_label.config(text=resources[self.language]['Exception2'])

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--model_path",
        "-m",
        type=str,
        default="../model",
        help="path to the trained model|ເສັ້ນທາງໄປສູ່ຕົວແບບທີ່ໄດ້ຮັບການຝຶກອົບຮົມ",
    )
    parser.add_argument(
        "-lao",
        action="store_true",

        help="show user interface in Lao|ສະແດງສ່ວນຕິດຕໍ່ຜູ້ໃຊ້ເປັນພາສາລາວ",
    )
    args = parser.parse_args()
    root = tk.Tk()
    app = App(root, args.model_path, args.lao)
    root.mainloop()