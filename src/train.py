import tensorflow as tf
import tensorflow_datasets as tfds
import tensorflow_addons as tfa
from tensorflow.keras.applications.efficientnet_v2 import EfficientNetV2B1 as EfficientNet
import random
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import accuracy_score, recall_score, precision_score

# 请根据内存容量调整BATCH_SIZE值. 
# BATCH_SIZE数值越小，占用内存越小，但训练速度也会减慢
# BATCH_SIZE的值建议设为4的倍数，最小值为4
# Please adjust the BATCH_SIZE value according to the memory capacity of your PC
# The smaller the BATCH_SIZE value, the less memory it requires, but the training speed will also slow down.
# It is recommended that the value of BATCH_SIZE be set to a multiple of 4, and the minimum value is 4
# ກະລຸນາປັບ BATCH_SIZE ຕາມຄວາມຈຸຂອງໜ່ວຍຄວາມຈຳຂອງ PC
# ຄ່າ BATCH_SIZE ນ້ອຍກວ່າ, ຄວາມຈຳໜ້ອຍມັນຕ້ອງການ, ແຕ່ຄວາມໄວໃນການຝຶກອົບຮົມຈະຊ້າລົງ.
# ແນະນຳໃຫ້ຕັ້ງ BATCH_SIZE ເປັນຄູນ 4, ແລະຄ່າຕໍ່າສຸດແມ່ນ 4.
BATCH_SIZE = 4
LEARNING_RATE = 0.0001
IMAGE_SIZE = [300,300]
EPOCHS=100 

# 训练数据扩增参数
# Training data augmentation parameters
# ພາລາມິເຕີທີ່ໃຊ້ເພື່ອຂະຫຍາຍຂໍ້ມູນການຝຶກອົບຮົມ
factor_translate_x = 0.13627885076064908
factor_translate_y = 0.1932223983981165
factor_contrast = 0.11786259978033162
factor_rotate= 0.1298624985539225

#设置数据集名称
# Name of the training dataset
# ຊື່ຊຸດຂໍ້ມູນ
dataset_name = 'oxford_flowers102'
#加载数据集
# load dataset
# ຊຸດຂໍ້ມູນການໂຫຼດ
dataset = tfds.load(name=dataset_name, data_dir="../flower_data/", split=['all'], with_info=True, as_supervised=True)
dataset, metadata = dataset
dataset=dataset[0]

#根据数据集大小确定训练、验证、测试集的大小
#训练、验证、测试集的大型分别是数据集的80%，10%，10%大小
# Split dataset to train set(80%), validation set(10%) and test set(10%)
# ແຍກຊຸດຂໍ້ມູນເພື່ອຝຶກອົບຮົມຊຸດ (80%), ຊຸດກວດສອບ (10%) ແລະຊຸດທົດສອບ (10%)
train_size = int(0.8 *len(dataset))
val_size = int(0.1 * len(dataset))
test_size = int(0.1 * len(dataset))
#对数据集进行随机打乱，使用随机数种子以保证可重复性
# Shuffle the data with a random seed to ensure repeatability
# ສຸ່ມລໍາດັບຂອງຂໍ້ມູນ, ການນໍາໃຊ້ເມັດຈໍານວນ ສຸ່ມເພື່ອຮັບປະກັນການເຮັດຊ້ໍາ
dataset = dataset.shuffle(2048, seed=2023)
#将打乱后的数据集按照比例分割成训练、验证、测试集
# Split the dataset
# ແຍກຊຸດຂໍ້ມູນ
train_dataset = dataset.take(train_size)
test_dataset = dataset.skip(train_size)
val_dataset = test_dataset.skip(test_size)
test_dataset = test_dataset.take(test_size)
#计算每个epoch的步数
# Calculate the number of steps for each epoch
# ຄິດໄລ່ຈໍານວນຂັ້ນຕອນໃນແຕ່ລະ epoch
STEPS_PER_EPOCH = len(train_dataset)//BATCH_SIZE
#训练过程中保存的当前最佳模型的路径及文件名
# The path and file name of the best model saved during the training process
# ເສັ້ນທາງແລະຊື່ໄຟລ໌ຂອງຕົວແບບທີ່ດີທີ່ສຸດທີ່ບັນທຶກໄວ້ໃນລະຫວ່າງຂະບວນການຝຶກອົບຮົມ
cp_path = '../checkpoints/best_weights.hdf5'
#在模型训练过程中，监控模型在验证集上的表现，如果当前epoch的表现比之前的表现更好，就将模型权重保存到指定路径下
# monitor the model performance on the validation set in training. If the performance of the current epoch is better than the previous, save the model weights to the specified path.
# ໃນລະຫວ່າງຂະບວນການຝຶກອົບຮົມແບບຈໍາລອງ, ຕິດຕາມການປະຕິບັດຂອງຕົວແບບໃນຊຸດການກວດສອບ, ຖ້າການປະຕິບັດຂອງ epoch ໃນປັດຈຸບັນແມ່ນດີກ່ວາການປະຕິບັດທີ່ຜ່ານມາ, ປະຫຍັດນ້ໍາຫນັກຕົວແບບໄປສູ່ເສັ້ນທາງທີ່ກໍານົດໄວ້.
cp_callback = tf.keras.callbacks.ModelCheckpoint(cp_path, save_best_only=True, save_weights_only=True)
#在模型训练过程中，动态地调整学习率，如果当前epoch的验证集loss已经连续5个epoch没有提升，就将当前学习率乘以0.2，即降低学习率
# dynamically update the learning rate, when the validation loss does not improve for 5 consecutive epochs, multiply the current learning rate by 0.2, that is, reduce the learning rate.
# ໃນລະຫວ່າງຂະບວນການຝຶກອົບຮົມແບບຈໍາລອງ, ອັດຕາການຮຽນຮູ້ຈະຖືກປັບຕົວແບບເຄື່ອນໄຫວ, ຖ້າການສູນເສຍຊຸດການຢັ້ງຢືນຂອງຍຸກປະຈຸບັນບໍ່ໄດ້ປັບປຸງ 5 ໄລຍະເວລາຕິດຕໍ່ກັນ, ຄູນອັດຕາການຮຽນຮູ້ໃນປະຈຸບັນໂດຍ 0.2, ນັ້ນແມ່ນ, ຫຼຸດຜ່ອນອັດຕາການຮຽນຮູ້.
lr_callback = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=5, min_lr=1e-6)
#在模型训练过程中，如果当前epoch的验证集表现已经连续10个epoch没有提升，就提前终止训练
# stop training if the performance does not improve for 10 consecutive epochs
# ໃນໄລຍະການຝຶກອົບຮົມຕົວແບບ, ຖ້າການຢັ້ງຢືນການປະຕິບັດຂອງຍຸກປະຈຸບັນຍັງບໍ່ໄດ້ຮັບການປັບປຸງ 10 ໄລຍະຕິດຕໍ່ກັນ, ການຝຶກອົບຮົມຈະຖືກຍົກເລີກໄວ.
es_callback = tf.keras.callbacks.EarlyStopping(
            # Stop training when `val_sparse_categorical_accuracy` is no longer improving
            monitor="val_loss",
            # "no longer improving" being defined as "no better than 1e-2 less"
            min_delta=1e-3,
            # "no longer improving" being further defined as "for at least 2 epochs"
            patience=10,
        )
pre_trained_model = EfficientNet(include_top=False, weights= 'imagenet', input_shape=(*IMAGE_SIZE,3))

def train_preprocessing(image, label):
    image = tf.cast(image, tf.float32)
    image = tf.image.resize_with_pad(image, IMAGE_SIZE[1], IMAGE_SIZE[0])
    # 图像扩增
    # image augmentation
    # ຮູບພາບອຸດົມສົມບູນ
    if tf.random.uniform(()) > 0.5:
        # 随机翻转图像
        # Randomly flip images
        # ພິກຮູບແບບສຸ່ມ
        image = tf.image.random_flip_left_right(image)
        # 随机调整色彩饱和度
        # Randomly adjust color saturation
        # ປັບຄວາມອີ່ມຕົວຂອງສີແບບສຸ່ມ
        image = tf.image.random_saturation(image, lower= 0, upper=5)
        # 随机调整亮度
        # Randomly adjust brightness
        # ປັບຄວາມສະຫວ່າງແບບສຸ່ມ
        image = tf.image.random_brightness(image, 0.2)
        # 随机调整对比度
        # Randomly adjust contrast
        # ປັບຄວາມຄົມຊັດແບບສຸ່ມ
        image = tf.image.random_contrast(image, 0, factor_contrast)
        # 随机上下左右移动图像
        # Randomly shift image
        # ເລື່ອນຮູບຂຶ້ນ, ລົງ, ຊ້າຍ ແລະຂວາ
        max_translation_x = factor_translate_x * image.shape[1]
        max_translation_y = factor_translate_y * image.shape[0]
        random_translation_x = random.uniform(-max_translation_x, max_translation_x)
        random_translation_y = random.uniform(-max_translation_y, max_translation_y)
        image = tfa.image.translate(image, [random_translation_x, random_translation_y] , fill_value=0) 
    # 使用 EfficientNet 的预处理函数对图像进行预处理
    # Preprocess the image using EfficientNet's preprocessing function
    # Preprocess ຮູບພາບໂດຍໃຊ້ຟັງຊັນ preprocessing ຂອງ EfficientNet        
    image = tf.keras.applications.efficientnet.preprocess_input(image)
    # 返回图像和标签
    # Return images and labels
    # ກັບຄືນຮູບພາບແລະປ້າຍຊື່
    return image, label

def val_preprocessing(image, label):
    # 将图像转换为 float32 类型
    # Change the image to float32 type
    # ປ່ຽນຮູບເປັນປະເພດ float32
    image = tf.cast(image, tf.float32) 
    # 调整图像大小并进行填充，使其形状符合 IMAGE_SIZE
    # Resize and pad the image to IMAGE_SIZE
    # ປັບຂະໜາດ ແລະຕື່ມຮູບເພື່ອໃຫ້ຮູບຮ່າງຂອງມັນກົງກັບ IMAGE_SIZE
    image = tf.image.resize_with_pad(image, IMAGE_SIZE[1], IMAGE_SIZE[0])   
    # 使用 EfficientNet 的预处理函数对图像进行预处理
    # Use EfficientNet's preprocessing function to preprocess the image
    # ໃຊ້ຟັງຊັນການປະມວນຜົນກ່ອນຂອງ EfficientNet ເພື່ອປະມວນຜົນຮູບກ່ອນ
    image = tf.keras.applications.efficientnet.preprocess_input(image)
    # 返回图像和标签
    # Return images and labels
    # ກັບຄືນຮູບພາບແລະປ້າຍຊື່
    return image, label

def solution_model():
    # 对训练集进行预处理
    # Preprocess the training set
    # Preprocess ຊຸດການຝຶກອົບຮົມ
    train_data= train_dataset.map(train_preprocessing).shuffle(2048).repeat(EPOCHS).batch(BATCH_SIZE).prefetch(tf.data.AUTOTUNE)
    # 对验证集进行预处理
    # Preprocess the validation set
    # ປະມວນຜົນຊຸດກວດສອບກ່ອນ
    val_data= val_dataset.map(val_preprocessing).batch(BATCH_SIZE).prefetch(tf.data.AUTOTUNE)
    # 构建模型
    # Build model
    # ສ້າງແບບຈໍາລອງ
    model = tf.keras.Sequential(
        [
            pre_trained_model,
            tf.keras.layers.GlobalAveragePooling2D(),
            tf.keras.layers.Dropout(0.6),
            tf.keras.layers.Dense(units=metadata.features["label"].num_classes)
        ], name = "flower_model"
    )
    # 编译模型
    # Compile model
    # ລວບລວມຕົວແບບ
    model.compile(optimizer=tf.optimizers.Adam(learning_rate=LEARNING_RATE),  # 选择优化器和学习率, set optimizer and learning rate, ເລືອກ optimizer ແລະອັດຕາການຮຽນຮູ້
                  loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True), # 选择损失函数, set loss, ເລືອກຟັງຊັນການສູນເສຍ
                  metrics=['sparse_categorical_accuracy']) # 选择评估指标, select evaluation metrics, ເລືອກ​ການ​ວັດ​ແທກ​ການ​ປະ​ເມີນ​ຜົນ​
    model.summary()

    # 尝试加载之前训练的权重
    # Try to load the weight from previous training
    # ພະຍາຍາມໂຫຼດນ້ໍາຫນັກທີ່ຝຶກອົບຮົມຜ່ານມາ
    try:
        model.load_weights(cp_path) 
        print('从之前训练的权重继续 !!!')
    except:
        pass
    
    # 训练模型
    # Train the model
    # ຮູບແບບລົດໄຟ
    history = model.fit(train_data, steps_per_epoch=STEPS_PER_EPOCH, epochs=EPOCHS, verbose=1, validation_data=val_data, callbacks=[cp_callback, lr_callback, es_callback])
    
    # 返回训练后的模型和训练过程历史
    # Return the trained model and training history
    # ກັບຄືນຮູບແບບການຝຶກອົບຮົມແລະປະຫວັດສາດຂະບວນການຝຶກອົບຮົມ
    return model, history

def plot_train_history(history):
    #plt.rcParams["font.sans-serif"]=["SimHei"] #设置字体
    plt.rcParams["axes.unicode_minus"]=False #该语句解决图像中的“-”负号的乱码问题
    # 绘制误差曲线图
    # Plot loss curve graph
    # ແຕ້ມເສັ້ນໂຄ້ງຄວາມຜິດພາດ
    plt.figure()
    plt.ylabel("训练和验证损失|train & validation loss")
    plt.xlabel("训练轮数|epochs")
    # x 轴使用整数刻度
    # Use integer scale for x-axis
    # ໃຊ້ຂະໜາດຈຳນວນເຕັມສຳລັບແກນ x
    plt.gca().xaxis.set_major_formatter(plt.matplotlib.ticker.FormatStrFormatter('%d')) 
    # 设置 y 轴范围
    # Set the y-axis range
    # ຕັ້ງແກນ y
    plt.ylim([0,max(history["loss"]+history["val_loss"])*1.1,]) 
    plt.plot(history["loss"])
    plt.plot(history["val_loss"])
    plt.legend()  
    plt.savefig('../plot/loss.png')  
    # 绘制准确率曲线图
    # Plot accuracy curve
    # ເສັ້ນໂຄ້ງຄວາມຖືກຕ້ອງ
    plt.figure()
    plt.ylabel("训练和验证准确率|accuracy")
    plt.xlabel("训练轮数|epochs")
    plt.ylim([0,max(history["sparse_categorical_accuracy"]+history["val_sparse_categorical_accuracy"])*1.1]) 
    plt.plot(history["sparse_categorical_accuracy"])
    plt.plot(history["val_sparse_categorical_accuracy"])
    plt.legend()  
    plt.savefig('../plot/accuracy.png') 

def test(model):
    y_true = []
    y_pred = []
    test_data= test_dataset.map(val_preprocessing).batch(1).prefetch(tf.data.AUTOTUNE)
    # 在测试集上测试模型的预测能力
    # Test the model on the test set
    # ທົດສອບຕົວແບບໃນຊຸດທົດສອບ
    for image, label in test_data:
        logits = model(image)
        probs = np.exp(logits.numpy()) / np.sum(np.exp(logits.numpy()), axis=-1, keepdims=True)
        probs = tf.squeeze(probs).numpy()
        y_pred.append(np.argmax(probs, axis=-1))
        y_true.append(label.numpy()[0])
    # 将预测值与真值计算模型评价值
    # Calculate the model metrics
    # ຄິດ​ໄລ່​ຕົວ​ຊີ້​ວັດ​ຕົວ​ແບບ​
    accuracy = accuracy_score(y_true, y_pred)
    recall = recall_score(y_true, y_pred, average='weighted')
    precision = precision_score(y_true, y_pred, average='weighted')
    return accuracy, recall, precision

if __name__ == '__main__':
    import pickle
    # 主程序入口，当模块被直接执行时执行以下代码
    # Main program entry
    # ການ​ເຂົ້າ​ໂຄງ​ການ​ຕົ້ນ​ຕໍ​
    print("开始训练模型|Start model training|ເລີ່ມ​ຕົວ​ແບບ​ການ​ຝຶກ​ອົບ​ຮົມ​...")
    # 调用 solution_model() 函数训练模型，获取模型和历史数据
    # Call the solution_model() function to train the model and get training history
    # ໂທຫາການທໍາງານຂອງ solution_model() ເພື່ອຝຶກອົບຮົມແບບຈໍາລອງແລະໄດ້ຮັບຕົວແບບແລະຂໍ້ມູນປະຫວັດສາດ
    model, history = solution_model()
    # 保存模型到指定路径
    # Save the model to the specified path
    # ບັນທຶກຕົວແບບເຂົ້າໄປໃນເສັ້ນທາງທີ່ກໍານົດ
    print("模型训练结束，保存模型|Model training is completed, save the model|ການຝຶກອົບຮົມແບບຈໍາລອງແມ່ນສໍາເລັດ, ຊ່ວຍປະຢັດຕົວແບບ...")
    model.save('../model') 
    # 使用 pickle 序列化保存历史数据到指定路径
    # Use pickle to save historical data to a specified path
    # ໃຊ້ດອງເພື່ອບັນທຶກຂໍ້ມູນປະຫວັດສາດໃສ່ເສັ້ນທາງທີ່ລະບຸໄວ້
    print("保存模型训练历史并绘制历史曲线图|Save model training history and draw historical curves|ບັນທຶກປະຫວັດການຝຶກອົບຮົມແບບຈໍາລອງແລະແຕ້ມເສັ້ນໂຄ້ງປະຫວັດສາດ...")
    with open('../history/HistoryDict.pkl', 'wb') as f:
        pickle.dump(history.history, f)    
    # 绘制训练历史曲线图
    # Plot training history curve graph
    # ແຕ້ມເສັ້ນໂຄ້ງປະຫວັດການຝຶກອົບຮົມ
    plot_train_history(history.history)
    print("测试模型质量|Test model quality|ທົດສອບຄຸນນະພາບຂອງຕົວແບບ...")
    accuracy, recall, precision = test(model)
    print("准确率|Accuracy:", accuracy)
    print("召回率|Recall:", recall)
    print("精确率|Precision:", precision)
    print("完成|Finish|ຈົບ!")