##### 以管理员权限打开 Windows Powershell 窗口 | Open Windows Powershell  with "Run as administrator" | ເປີດ Windows Powershell ດ້ວຍ "Run as administrator"
<div style="display: flex; justify-content: center;">
  <div style="text-align: center; margin-right: 20px;">
    <img src="readme_images/powershell.png" alt="Open Windows Powershell" style="display: block; margin-left: auto; margin-right: auto;"/>
    <p class="text-center" style="text-align: center;">图1：打开Windows Powershell<br>Open Windows Powershell<br>ເປີດ Windows Powershell</p>
  </div>
  <div style="text-align: center;">
    <img src="readme_images/powershell2.png" alt="Image 2" style="display: block; margin-left: auto; margin-right: auto;"/>
    <p class="text-center" style="text-align: center;">图2：打开的 Windows Powershell窗口<br>A Windows Powershell window opened<br>ປ່ອງຢ້ຽມ Windows Powershell ແລ່ນຢູ່</p>
  </div>
</div>
<br>

##### 下载 git 工具 | Download the Git tool | ດາວໂຫລດເຄື່ອງມື git

前往 https://gitforwindows.org/ 下载git 工具并安装
Go to https://gitforwindows.org/ to download the git tool and install it
ໄປທີ່ https://gitforwindows.org/ ເພື່ອດາວໂຫລດເຄື່ອງມື git ແລະຕິດຕັ້ງມັນ
<br>

##### 下载深度学习课程代码 | Download this repo | ດາວໂຫລດລະຫັດຫຼັກສູດການຮຽນຮູ້ເລິກ
打开Windows Powershell，依次输入以下命令:
Open Windows Powershell and enter the following commands in sequence:
ເປີດ Windows Powershell ແລະໃສ່ຄໍາສັ່ງຕໍ່ໄປນີ້ຕາມລໍາດັບ:
```bash
PS C:\> mkdir -p c:\ict-class
PS C:\> cd c:\ict-class
PS C:\ict-class> git clone https://gitlab.com/ict-class/deep-learning
PS C:\ict-class> cd deep-learning\flower_data\oxford_flowers102\2.1.1
PS C:\ict-class\deep-learning\flower_data\oxford_flowers102\2.1.1> Get-Content -Path oxford_flowers102-test.tfrecord-00000-of-00002-partaa, oxford_flowers102-test.tfrecord-00000-of-00002-partab, oxford_flowers102-test.tfrecord-00000-of-00002-partac -Raw | Set-Content -Path oxford_flowers102-test.tfrecord-00000-of-00002
PS C:\ict-class\deep-learning\flower_data\oxford_flowers102\2.1.1> Get-Content -Path oxford_flowers102-test.tfrecord-00001-of-00002-partaa, oxford_flowers102-test.tfrecord-00001-of-00002-partab, oxford_flowers102-test.tfrecord-00001-of-00002-partac -Raw | Set-Content -Path oxford_flowers102-test.tfrecord-00001-of-00002
PS C:\ict-class\deep-learning\flower_data\oxford_flowers102\2.1.1> Remove-Item -Path oxford_flowers102-test.tfrecord-00000-of-00002-parta*, oxford_flowers102-test.tfrecord-00001-of-00002-parta*
PS C:\ict-class\deep-learning\flower_data\oxford_flowers102\2.1.1> dir
```
目录下的文件应该显示为：
```bash
    Directory: C:\ict-class\deep-learning\flower_data\oxford_flowers102\2.1.1

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----         2024/3/13     11:29          55754 dataset_info.json
-a----         2024/3/13     11:29            971 features.json
-a----         2024/3/13     11:29           1301 label.labels.txt
-a----         2024/3/13     11:30      130171933 oxford_flowers102-test.tfrecord-00000-of-00002
-a----         2024/3/13     11:30      130711328 oxford_flowers102-test.tfrecord-00001-of-00002
-a----         2024/3/13     11:30       43490904 oxford_flowers102-train.tfrecord-00000-of-00001
-a----         2024/3/13     11:30       43196598 oxford_flowers102-validation.tfrecord-00000-of-00001
```
<br>

##### 安装实验相关软件 | Install lab software | ຕິດຕັ້ງຊອບແວທີ່ກ່ຽວຂ້ອງກັບການທົດລອງ
###### 安装Python | Install Python | ຕິດຕັ້ງ Python
<div style="background-color:lightyellow; padding:4px; border:1px solid yellow;"><span style="color:orange">&#9888;</span> 如果你已经安装了Python，可以跳过这一步<br>
If you already have Python installed, you can skip this step<br>ຖ້າທ່ານມີ Python ຕິດຕັ້ງແລ້ວ, ທ່ານສາມາດຂ້າມຂັ້ນຕອນນີ້
</div>
<br>

在Windows Powershell中，依次输入以下命令,运行python安装程序. 根据python安装程序的提示完成安装。
In Windows Powershell, enter the following commands in to run the python installer. Complete the installation by following the installer steps.
ໃນ Windows Powershell, ໃສ່ຄໍາສັ່ງຕໍ່ໄປນີ້ເພື່ອດໍາເນີນການຕິດຕັ້ງ python.ສໍາເລັດການຕິດຕັ້ງໂດຍປະຕິບັດຕາມຂັ້ນຕອນການຕິດຕັ້ງ.
```bash
PS C:\ict-class\deep-learning> cd C:\ict-class\deep-learning\software
PS C:\ict-class\deep-learning\software> python-3.9.13-amd64.exe
```
在安装python的过程中，请选择"Add Python 3.9 to Path"选项:
During the installation of python, please select the "Add Python 3.9 to Path" option:
ໃນລະຫວ່າງການຕິດຕັ້ງ python, ກະລຸນາເລືອກຕົວເລືອກ "Add Python 3.9 to Path":
<div style="text-align: left;">
  <img src="readme_images/python.png" alt="Image" style="display: block; margin-left: auto; margin-right: auto;"/>
  <p style="text-align: center;">图2：在安装过程中选择"Add Python 3.9 to Path"<br>Select "Add Python 3.9 to Path" during installation<br>ເລືອກ "Add Python 3.9 to Path" ໃນລະຫວ່າງການຕິດຕັ້ງ</p>
</div>


在Windows Powershell中，运行以下命令，确认python安装成功：
In Windows Powershell, run the following command to confirm that python is installed successfully:
ໃນ Windows Powershell, ດໍາເນີນການຄໍາສັ່ງຕໍ່ໄປນີ້ເພື່ອຢືນຢັນວ່າ python ຖືກຕິດຕັ້ງຢ່າງສໍາເລັດຜົນ

```bash
PS C:\ict-class\deep-learning\software> .\python.exe --version
```

以上命令的输出为：
The output of the above command is:
ຜົນຜະລິດຂອງຄໍາສັ່ງຂ້າງເທິງນີ້ແມ່ນ

```bash
Python 3.9.13 
```
<br>

###### 安装pip工具 | Install pip tool | ຕິດຕັ້ງເຄື່ອງມື pip
<div style="background-color:lightyellow; padding:4px; border:1px solid yellow;"><span style="color:orange">&#9888;</span> 如果你已经安装pip工具，可以跳过这一步<br>If you have already installed the pip tool, you can skip this step<br>ຖ້າທ່ານໄດ້ຕິດຕັ້ງເຄື່ອງມື pip ແລ້ວ, ທ່ານສາມາດຂ້າມຂັ້ນຕອນນີ້</div>
<br>

在Windows Powershell中，输入以下命令：
In Windows Powershell, enter the following command:
ໃນ Windows Powershell, ໃສ່ຄໍາສັ່ງຕໍ່ໄປນີ້:

```bash
PS C:\ict-class\deep-learning\software> python.exe get-pip.py
```

###### 安装 python工具包 | Install python packages | ຕິດຕັ້ງຊຸດເຄື່ອງມື python
在Windows Powershell中，依次输入以下命令:
In Windows Powershell, enter the following commands in sequence:
ໃນ Windows Powershell, ໃສ່ຄໍາສັ່ງຕໍ່ໄປນີ້ຕາມລໍາດັບ:

```bash
PS C:\ict-class\deep-learning\software> cd ..
PS C:\ict-class\deep-learning> pip install -r requirements.txt
```

#### 运行模型训练 | Run model training | ດໍາເນີນການຝຶກອົບຮົມແບບຈໍາລອງ
在Windows Powershell中，通过以下命令启动模型训练:
In Windows Powershell, start model training with the following command:
ໃນ Windows Powershell, ເລີ່ມການຝຶກອົບຮົມແບບຈໍາລອງດ້ວຍຄໍາສັ່ງຕໍ່ໄປນີ້:
```bash
PS C:\ict-class\deep-learning> cd src
PS C:\ict-class\deep-learning\src> python.exe train.py
```

<div style="background-color:lightyellow; padding:4px; border:1px solid yellow;"><span style="color:orange">&#9888;</span>完成模型训练，需要较长时间（在不同的硬件系统上，模型训练的时间会有所差异。完成训练通常需要等待几十分钟到几小时）。如果不希望等待模型训练完成。可以使用预先训练好的模型继续下面的步骤。预训练的模型可以在 C:\ict-class\deep-learning\pre-trained_flower_model 目录下找到。<br>It usually takes tens of minutes to several hours to complete the training. If you don't want to wait for model training to complete. You can use the pre-trained model to continue with the next steps.The pretrained model can be found in the C:\ict-class\deep-learning\pre-trained_flower_model directory.<br> ໂດຍປົກກະຕິແລ້ວ ມັນຈະໃຊ້ເວລາຫຼາຍສິບນາທີຫາຫຼາຍຊົ່ວໂມງເພື່ອເຮັດສຳເລັດການຝຶກອົບຮົມ. ຖ້າທ່ານບໍ່ຕ້ອງການລໍຖ້າການຝຶກອົບຮົມແບບຈໍາລອງສໍາເລັດ. ທ່ານສາມາດນໍາໃຊ້ແບບຈໍາລອງທາງສ່ວນຫນ້າຂອງການຝຶກອົບຮົມເພື່ອສືບຕໍ່ຂັ້ນຕອນຕໍ່ໄປ.ຮູບແບບທີ່ຝຶກໄວ້ລ່ວງໜ້າສາມາດພົບໄດ້ໃນບັນຊີ C:\ict-class\deep-learning\pre-trained_flower_model.</div>
<br>

<div style="background-color:lightyellow; padding:4px; border:1px solid yellow;"><span style="color:orange">&#9888;</span> 如果电脑内存足够大，可尝试用文本编辑器修改C:\ict-class\deep-learning\src\train.py文件的第19行。将BATCH_SIZE值修改为8、12、16或更大的值。更大的BATCH_SIZE值可加快训练时间，但需消耗更大内存空间<br>If the computer memory is large enough, try using a text editor to modify line 19 of the C:\ict-class\deep-learning\src\train.py file. Modify the BATCH_SIZE value to 8, 12, 16 or greater. A larger BATCH_SIZE value can speed up training time, but requires more memory space.<br>ຖ້າໜ່ວຍຄວາມຈຳຂອງຄອມພິວເຕີມີຂະໜາດໃຫຍ່ພໍ, ລອງໃຊ້ຕົວແກ້ໄຂຂໍ້ຄວາມເພື່ອແກ້ໄຂແຖວທີ 19 ຂອງໄຟລ໌ C:\ict-class\deep-learning\src\train.py. ແກ້ໄຂຄ່າ BATCH_SIZE ເປັນ 8, 12, 16 ຫຼືຫຼາຍກວ່ານັ້ນ. ຄ່າ BATCH_SIZE ທີ່ໃຫຍ່ກວ່າສາມາດເລັ່ງເວລາການຝຶກອົບຮົມໄດ້, ແຕ່ຕ້ອງການພື້ນທີ່ຄວາມຈຳຫຼາຍ.</div>
<br>

模型训练完成后，将显示准确率等结果(输出的数值可能略有差异)。
After the model training is completed, it prints the model quality metrics (the output values may be slightly different).
ຫຼັງ​ຈາກ​ການ​ຝຶກ​ອົບ​ຮົມ​ຕົວ​ແບບ​ໄດ້​ສໍາ​ເລັດ​, ມັນ​ພິມ​ມາດ​ຕະ​ການ​ຄຸນ​ນະ​ພາບ​ຂອງ​ຕົວ​ແບບ (ຄ່າ​ຜົນ​ຜະ​ລິດ​ອາດ​ຈະ​ແຕກ​ຕ່າງ​ກັນ​ເລັກ​ນ້ອຍ​)​.
```bash
...
准确率|Accuracy: 0.9987775061124694
召回率|Recall: 0.9987775061124694
精确率|Precision: 0.9989133387666395
完成|Finish|ຈົບ!
```

同时，在 C:\ict-class\deep-learning\plot 目录下可以看到accuracy.png和loss.png 2个图像文件.
Two image files, accuracy.png and loss.png, can be seen in the C:\ict-class\deep-learning\plot directory.
ສອງໄຟລ໌ຮູບພາບ, accuracy.png ແລະ loss.png, ສາມາດເຫັນໄດ້ໃນ C:\ict-class\deep-learning\plot directory.
```bash
PS C:\ict-class\deep-learning\src> cd ..\plot
PS C:\ict-class\deep-learning\plot> dir

    Directory: C:\ict-class\deep-learning\plot

Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----         2024/3/12     23:28          20132 accuracy.png
-a----         2024/3/12     23:28          21209 loss.png
```

这2个文件分别显示了在训练过程中模型预测的准确率逐渐提高，最后接近100%。同时模型的预测的误差逐渐减小的趋势。
These two files respectively show that the accuracy of model prediction gradually increases during the training process, and finally approaches ~100%. At the same time, the prediction error(loss) of the model gradually decreases.
ສອງໄຟລ໌ນີ້ຕາມລໍາດັບສະແດງໃຫ້ເຫັນວ່າຄວາມຖືກຕ້ອງຂອງການຄາດຄະເນຕົວແບບຄ່ອຍໆເພີ່ມຂຶ້ນໃນລະຫວ່າງຂະບວນການຝຶກອົບຮົມ, ແລະສຸດທ້າຍໄດ້ເຂົ້າຫາ ~100%. ໃນເວລາດຽວກັນ, ຄວາມຜິດພາດຂອງການຄາດຄະເນຂອງຕົວແບບຄ່ອຍໆຫຼຸດລົງ.
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Side-by-Side Images (Bootstrap)</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <img src="plot/accuracy.png" alt="Image 1" class="img-fluid">
        <p class="text-center" style="text-align: center;">图3 accuracy.png 显示了在训练过程中模型预测的准确率逐渐提高，最后接近100%<br>accuracy.png shows that the accuracy of model predictions gradually increases during the training process, and finally approaches ~100%.<br>accuracy.png ສະແດງໃຫ້ເຫັນວ່າຄວາມຖືກຕ້ອງຂອງການຄາດຄະເນຕົວແບບຄ່ອຍໆເພີ່ມຂຶ້ນໃນລະຫວ່າງຂະບວນການຝຶກອົບຮົມ, ແລະສຸດທ້າຍໄດ້ເຂົ້າຫາ ~100%.</p>
      </div>
      <div class="col-md-6">
        <img src="plot/loss.png" alt="Image 2" class="img-fluid">
        <p class="text-center" style="text-align: center;">图4 loss.png 模型的预测的误差逐渐减小的趋势<br>loss.png shows Model's prediction error is gradually decreasing<br>loss.png ສະແດງໃຫ້ເຫັນຄວາມຜິດພາດການຄາດຄະເນຂອງຕົວແບບແມ່ນຄ່ອຍໆຫຼຸດລົງ</p>
      </div>
    </div>
  </div>
</body>


#### 运行模型推理 | Run model inference | ໃຊ້ຕົວແບບເພື່ອເຮັດໃຫ້ການຄາດຄະເນ
在Windows Powershell中，通过以下命令启动模型推理工具：(选项 -lao 参数启用了老挝文字界面。如何忽略该选项，界面中的文字显示为英语)
In Windows Powershell, start the model inference tool with the following command: (The option -lao parameter enables the Lao interface. If you ignore this option, the text in the interface is displayed in English)
ໃນ Windows Powershell, ເລີ່ມຕົ້ນເຄື່ອງມື inference ແບບຈໍາລອງດ້ວຍຄໍາສັ່ງຕໍ່ໄປນີ້: (ທາງເລືອກ -lao ພາລາມິເຕີເຮັດໃຫ້ການໂຕ້ຕອບຂໍ້ຄວາມລາວ. ຖ້າທ່ານບໍ່ສົນໃຈທາງເລືອກນີ້, ຂໍ້ຄວາມໃນການໂຕ້ຕອບຈະສະແດງເປັນພາສາອັງກິດ).
```bash
PS C:\ict-class\deep-learning\plot> cd ..\src
PS C:\ict-class\deep-learning\src> python.exe run.py -lao
```

<div style="background-color:lightyellow; padding:4px; border:1px solid yellow;"><span style="color:orange">&#9888;</span>如果在模型训练步骤中未完成训练，可以用以下命令使用预先训练成功的模型运行推理工具

```bash
PS C:\ict-class\deep-learning\src> python.exe run.py -lao -m ../pre-trained_flower_model
```
</div>
<br>
模型推理工具启动以后的界面如下：
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Side-by-Side Images (Bootstrap)</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <img src="plot/accuracy.png" alt="Image 1" class="img-fluid">
        <p class="text-center" style="text-align: center;">图5-1 显示英文文字的用户界面<br>User interface in English</p>
      </div>
      <div class="col-md-6">
        <img src="plot/loss.png" alt="Image 2" class="img-fluid">
        <p class="text-center" style="text-align: center;">ຮູບ5-2 ສ່ວນຕິດຕໍ່ຜູ້ໃຊ້ໃນພາສາລາວ</p>
      </div>
    </div>
  </div>
</body>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Side-by-Side Images (Bootstrap)</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <img src="plot/accuracy.png" alt="Image 1" class="img-fluid">
        <p class="text-center" style="text-align: center;">图6-1 点击"Browse"按钮后出现图像选择窗口<br>After clicking the "Browse" button, an image selection window will appear​</p>
      </div>
      <div class="col-md-6">
        <img src="plot/loss.png" alt="Image 2" class="img-fluid">
        <p class="text-center" style="text-align: center;">ຮູບ 6-2 ຫຼັງ​ຈາກ​ທີ່​ຄລິກ​ໃສ່​ປຸ່ມ"ຄົ້ນຫາ"​, ປ່ອງ​ຢ້ຽມ​ເລືອກ​ຮູບ​ພາບ​ຈະ​ປາ​ກົດ​ຂຶ້ນ​</p>
      </div>
    </div>
  </div>
</body>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <img src="plot/accuracy.png" alt="Image 1" class="img-fluid">
        <p class="text-center" style="text-align: center;">图7-1 完成图像选择后的界面<br>User interface after completing image selection</p>
      </div>
      <div class="col-md-6">
        <img src="plot/loss.png" alt="Image 2" class="img-fluid">
        <p class="text-center" style="text-align: center;">ຮູບ 7-2 ການໂຕ້ຕອບຫຼັງຈາກສໍາເລັດການຄັດເລືອກຮູບພາບ</p>
      </div>
    </div>
  </div>
</body>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <img src="plot/accuracy.png" alt="Image 1" class="img-fluid">
        <p class="text-center" style="text-align: center;">图8-1 点击"Recognize"按钮后输出模型识别的结果<br>点击后需要稍等片刻才能显示结果。等待的时长取决于电脑的性能<br>After clicking the "Recognize" button, the result of model recognition is output<br>It will take some time for the results to be displayed. Latnecy depends on your computer's performance.</p>
      </div>
      <div class="col-md-6">
        <img src="plot/loss.png" alt="Image 2" class="img-fluid">
        <p class="text-center" style="text-align: center;">ຮູບ 8-2 ຫຼັງຈາກທີ່ຄລິກໃສ່ປຸ່ມ "ຮັບຮູ້", ຜົນໄດ້ຮັບຂອງການຮັບຮູ້ແບບຈໍາລອງແມ່ນຜົນຜະລິດ<br>ມັນຈະໃຊ້ເວລາໄລຍະໜຶ່ງເພື່ອໃຫ້ຜົນໄດ້ຮັບຖືກສະແດງ. latency ແມ່ນຂຶ້ນກັບປະສິດທິພາບຂອງຄອມພິວເຕີຂອງທ່ານ.</p>
      </div>
    </div>
  </div>
</body>
<br>

<div style="background-color:lightyellow; padding:4px; border:1px solid yellow;"><span style="color:orange">&#9888;</span>由于在模型训练中仅使用了花卉数据，模型仅能识别花卉。如输入其它类型的图像，将获得错误的识别结果。<br>Since only flower data was used in model training, the model can only recognize flowers. If you input other types of images, you will get incorrect recognition results.<br>ເນື່ອງຈາກຂໍ້ມູນດອກໄມ້ພຽງແຕ່ຖືກນໍາໃຊ້ໃນການຝຶກອົບຮົມຕົວແບບ, ຕົວແບບສາມາດຮັບຮູ້ດອກໄມ້ເທົ່ານັ້ນ. ຖ້າທ່ານໃສ່ຮູບພາບປະເພດອື່ນ, ທ່ານຈະໄດ້ຮັບຜົນການຮັບຮູ້ທີ່ບໍ່ຖືກຕ້ອງ.</div>
</html>

#### 结束 | End | ສິ້ນສຸດ